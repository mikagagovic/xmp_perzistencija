﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;


namespace Common
{
    public interface IWritter
    {
        bool Write(string ids,string content,string path);
        bool Read(string path);
        
        bool BulkWrite();

        bool FindElement();
    }
}
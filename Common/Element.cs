﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
   public class Element
    {
        private string id;
        private string content;

        public string Id { get => id; set => id = value; }
        public string Content { get => content; set => content = value; }

        public Element(string id, string content)
        {
            this.id = id;
            this.content = content;
        }
        public Element()
        {

        }
    }
}

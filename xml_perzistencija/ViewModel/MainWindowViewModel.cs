﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;
using System.Xml;
using Common;
using System.IO;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace xml_perzistencija.ViewModel
{
    public class MainWindowViewModel : INotifyPropertyChanged, IWritter
    {
        private static IWritter viewvmodel;
        private string filePath;
        private string readResult;
        private string writeContent;
        private string writeId;
        private ICommand openFileDialog;
        private ICommand readFromFile;
        private ICommand writeInFile;
        private ICommand saveFileDialog;
        private ICommand addToList;
        private ICommand bulkWrite;
        private ICommand findcommand;
        private BindingList<Element> list = new BindingList<Element>();
        Stream myStream = null;
        public static XDocument document;

        public static IWritter Instance
        {
            get
            {
                if (Viewvmodel == null)
                {
                    Viewvmodel = new MainWindowViewModel();
                }
                return viewvmodel;
            }
            set
            {
                if (Viewvmodel == null)
                {
                    viewvmodel = value;
                }
            }
        }
        public string FilePath
        {
            get { return filePath; }
            set
            {
                filePath = value;
                OnPropertyChanged("FilePath");
            }
        }
        public string WriteContent
        {
            get { return writeContent; }
            set
            {
                writeContent = value;
                OnPropertyChanged("WriteContent");
            }

        }
        public string WriteID
        {
            get { return writeId; }
            set
            {
                writeId = value;
                OnPropertyChanged("WriteID");
            }

        }
        public string ReadResult
        {
            get { return readResult; }
            set
            {
                readResult = value;
                OnPropertyChanged("ReadResult");
            }
        }
        public  BindingList<Element> bl
        {
            get { return list; }
            set { list = value; }
        }

        public ICommand OpenFileDialog
        {
            get
            {
                return openFileDialog ?? (openFileDialog = new RelayCommand(param => this.OpenFile()));
            }
        }

        public ICommand SaveFileDialog
        {
            get
            {
                return saveFileDialog ?? (saveFileDialog = new RelayCommand(param => this.SaveFileDialogMethod()));
            }
        }

        public ICommand ReadFromFile
        {
            get
            {
                return readFromFile ?? (readFromFile = new RelayCommand(param => this.Read(FilePath)));
            }
        }
        public ICommand WriteInFile
        {
            get
            {
                return writeInFile ?? (writeInFile = new RelayCommand(param => this.Write(WriteID, WriteContent,filePath)));
            }
        }

        public ICommand AddToList
        {
            get
            {
                return addToList ?? (addToList = new RelayCommand(param => this.AddTolist()));
            }
        }

        public ICommand BulkWriteCommand
        {
            get
            {
                return bulkWrite ?? (bulkWrite = new RelayCommand(param => this.BulkWrite()));
            }
        }

        public ICommand FindElementCommand
        {
            get
            {
                return findcommand ?? (findcommand = new RelayCommand(param => this.FindElement()));
            }
        }

        public static IWritter Viewvmodel { get => viewvmodel; set => viewvmodel = value; }

        private void AddTolist()
        {
            Element e = new Element(WriteID, WriteContent);
            bl.Add(e);
            WriteContent = "";
            WriteID = "";
        }

        private void OpenFile()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Title = "Open xml file..";
            openFileDialog.Filter = "XML Files|*.xml;|All Files|*.*";
            openFileDialog.RestoreDirectory = true;

            DialogResult dialogResponse = openFileDialog.ShowDialog();
            if (dialogResponse == DialogResult.OK)
            {
                FilePath = openFileDialog.FileName;
                //  myStream = openFileDialog.OpenFile();

            }
        }
        private void SaveFileDialogMethod()
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();

            saveFileDialog1.Title = "Open xml file..";
            saveFileDialog1.Filter = "XML Files|*.xml;|All Files|*.*";
            saveFileDialog1.RestoreDirectory = true;

            DialogResult dialogResponse = saveFileDialog1.ShowDialog();
            if (dialogResponse == DialogResult.OK)
            {
                FilePath = saveFileDialog1.FileName;

            }
        }


        public bool Write(string id, string content,string path)
        {
            XmlDocument doc = new XmlDocument();
            try
            {
                doc.Load(path);
            }
            catch (Exception)
            {

                return false;
            }

            XmlNode element = doc.CreateElement("Element");
            XmlNode idEl = doc.CreateElement("id");
            idEl.InnerText = id;
            XmlNode name = doc.CreateElement("name");
            name.InnerText = content;
            element.AppendChild(idEl); element.AppendChild(name);
            doc.DocumentElement.AppendChild(element);
            try
            {
                doc.Save(path);
            }
            catch (Exception)
            {

                return false;
                   
            }
            WriteContent = "";
            WriteID = "";


            return true;

        }

        public bool Read(string path)
        {
            try
            {
                
                    XmlDocument doc = new XmlDocument();
                    try
                    {
                        doc.Load(path);
                    }
                    catch (Exception e)
                    {
                        MessageBox.Show(e.ToString());
                    }

                    ReadResult = "";
                    foreach (XmlNode node in doc.DocumentElement.ChildNodes)
                    {
                        ReadResult += "\n<Element>";
                        ReadResult += "\n\t<id>";
                        ReadResult += node.FirstChild.LastChild.Value;
                        ReadResult += "\n\t<\\id>";
                        ReadResult += "\n\t<name>";
                        ReadResult += node.LastChild.LastChild.Value;
                        ReadResult += "\n\t<\\name>";
                        ReadResult += "\n<\\Element>";
                    }
                  
                    return true;
                
            }
            catch (Exception)
            {

                return false;
            }
           

        }
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

       
      public  bool BulkWrite()
        {
            if (bl.Count < 2)
            {
                MessageBox.Show("Need min 2 elements for writting");
                return false;
            }
            XmlDocument doc = new XmlDocument();
            try
            {
                doc.Load(filePath);
            }
            catch (Exception)
            {

                return false;
            }
            foreach (Element e in bl)
            {
                XmlNode element = doc.CreateElement("Element");
                XmlNode idEl = doc.CreateElement("id");
                idEl.InnerText = e.Id;
                XmlNode name = doc.CreateElement("name");
                name.InnerText = e.Content;
                element.AppendChild(idEl); element.AppendChild(name);
                doc.DocumentElement.AppendChild(element);
            }
            try
            {
                doc.Save(filePath);
            }
            catch (Exception)
            {

                return false;

            }
            bl.Clear();
            WriteContent = "";
            WriteID = "";

            return true;
        }

        public bool FindElement()
        {
            if (FilePath == "" || filePath == null)
            {
                return false;
            }
            using (myStream)
            {
                XmlDocument doc = new XmlDocument();
                try
                {
                    doc.Load(filePath);
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.ToString());
                }
                List<Element> pomList = new List<Element>();
                string readResult = "";
                foreach (XmlNode node in doc.DocumentElement.ChildNodes)
                {
                    readResult += "\n<Element>";
                    readResult += "\n\t<id>";
                    readResult += node.FirstChild.LastChild.Value;
                    readResult += "\n\t<\\id>";
                    readResult += "\n\t<name>";
                    readResult += node.LastChild.LastChild.Value;
                    readResult += "\n\t<\\name>";
                    readResult += "\n<\\Element>";
                    pomList.Add(new Element(node.FirstChild.LastChild.Value, node.LastChild.LastChild.Value));
                    
                }
                if (WriteContent != "" || WriteID != "")
                {
                    foreach (Element item in pomList)
                    {
                        if (item.Content == WriteContent)
                        {
                            MessageBox.Show("Element: id-jem" + item.Id+" and name "+ item.Content);
                            return true;
                        }
                        if (item.Id == WriteID)
                        {
                            MessageBox.Show("Element: id-jem" + item.Id + " and name " + item.Content);
                            return true;
                        }

                    }
                }

                return true;
            }

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NSubstitute;
using NUnit.Framework;
using xml_perzistencija.ViewModel;
using Common;

namespace ViewModelTest
{
    [TestFixture]
    public class IWriterTest
    {
        private MainWindowViewModel viewModel;
        string path =( System.IO.Path.GetDirectoryName(
             System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase) + "\\doc.xml").Substring(6);

        [OneTimeSetUp]
        public void SetupTest()
        {
            viewModel = new MainWindowViewModel();
            MainWindowViewModel.Instance  = Substitute.For<IWritter>();
           
            MainWindowViewModel.Instance.Read(path).Returns(true);
            MainWindowViewModel.Instance.Read(path+"path").Returns(false);
            MainWindowViewModel.Instance.Write("23", "Content",path).Returns(true);
            MainWindowViewModel.Instance.Write("3", "Content1",path+"pa").Returns(false);
            MainWindowViewModel.Instance.BulkWrite().Returns(true);
            MainWindowViewModel.Instance.FindElement().Returns(true);
        }
        [Test]
        public void ReadTestTrue()
        {
            bool result = viewModel.Read(path);
            Assert.IsTrue(result);
        }
        [Test]
        public void ReadTestFalse()
        {
            bool result = viewModel.Read(@path+"smothing");
            Assert.IsFalse(result);
        }
        public void WriteTestTrue()
        {
            bool result = viewModel.Write("3", "Content1",path);
            Assert.IsFalse(result);
        }

        [Test]
        public void WriteTestFalse()
        {
            bool result = viewModel.Write("3", "Content1",path+"pa");
            Assert.IsFalse(result);
        }

        [Test]
        public void BulkWrite()
        {
            bool result = viewModel.BulkWrite();
            Assert.IsFalse(result);
        }

        [Test]
        public void FindElement()
        {
            bool result = viewModel.FindElement();
            Assert.IsFalse(result);
        }

    }
}
